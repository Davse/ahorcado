var console = console || {},
  document = document || {},
                          
  Ahorcado = {
    imagenes: ['Ahorcado1', 'Ahorcado2', "Ahorcado3", "Ahorcado4", "Ahorcado5", "Ahorcado6", "Ahorcado7", "Ahorcado8"],
    palabras: ['programacion', 'ambiente', 'videoconferencia',' paralelepipedos','circunferencia','litofotograficamente','dimension','demasia','divisibilidad'],
    palabraElegida: "",
    teclas: "abcdefghijklmnopqrstuvwxyz",
    intentosFallidos: 0,
    iniciar: function (contenedor) {
      "use strict";
      var div = document.getElementById(contenedor);
      this.armaHTMLTitulo(div);
      this.armaHTMLDibujo(div);
      this.armaHTMLTeclado(div);
      this.botonReiniciar(div);        
      this.armaHTMLPalabra();
    },
    armaHTMLTitulo: function (div) {
      var eTitulo = document.createElement("h1"),
        tTitulo = document.createTextNode("Juego del Ahorcado");
      eTitulo.appendChild(tTitulo);
      div.appendChild(eTitulo);
    },
    armaHTMLDibujo: function (div) {
      var eSection = document.createElement("section"),
        eImg = document.createElement("img");
        eSection.id="picture";    
      eSection.class = "Ahorcado_patibulo";
      eSection.appendChild(eImg);
      div.appendChild(eSection);
      eImg.src = "images/" + this.imagenes[0] + ".png";
      eImg.id = "Ahorcado_imagenPatibulo";
    },
    armaHTMLPalabra: function () {
      this.palabraElegida = this.palabras[parseInt(Math.random() * this.palabras.length)];
      var eSection = document.createElement("section"),
        eTitulo = document.createElement("h2"),
        tTitulo = document.createTextNode("Palabra Buscada"),
        ePalabra = document.createElement("p"),
        tPalabra = document.createTextNode(this.palabraBuscada());
      ePalabra.id = "Ahorcado_palabraBuscada";
      eTitulo.appendChild(tTitulo);
      ePalabra.appendChild(tPalabra);
      eSection.appendChild(eTitulo);
      eSection.appendChild(ePalabra);
      document.getElementById("picture").appendChild(eSection);
    },
    
    palabraBuscada: function () {
      var teclasSinUsar = document.getElementsByClassName("Ahorcado_teclaSinUsar"),
        palabra = this.palabraElegida,
        regExp;
      //console.log(teclasSinUsar);
      for (var i = 0; i < teclasSinUsar.length; i++) {
        regExp = new RegExp(teclasSinUsar[i].innerHTML, "g");
        //console.log(regExp);
        palabra = palabra.replace(regExp, "_");
      }
      if (palabra === this.palabraElegida){
        document.getElementById("Ahorcado_imagenPatibulo").src = "images/AhorcadoWinner.png"
        document.getElementsByClassName("Ahorcado_teclado")[0].style.display="none";
              document.getElementById("reiniciar").style.display="block";
      }

      return palabra;
    },

    controlDeErrores: function(tecla){
      if (!this.palabraElegida.includes(tecla)) {
        this.intentosFallidos +=1;
        document.getElementById("Ahorcado_imagenPatibulo").src = "images/" + this.imagenes[this.intentosFallidos] + ".png"
          if (this.intentosFallidos === 7){ 
              document.getElementsByClassName("Ahorcado_teclado")[0].style.display="none";
              document.getElementById("reiniciar").style.display="block";
          }
      }
    },

      clickboton: function (event) {
          var event = event || e;
          event.target.classList.remove("Ahorcado_teclaSinUsar");
          event.target.classList.add("Ahorcado_teclaUsada");          
          document.getElementById("Ahorcado_palabraBuscada").innerHTML = Ahorcado.palabraBuscada();
          Ahorcado.controlDeErrores(event.target.innerHTML);
        },
      
      
    armaHTMLTeclado: function (div) {
      var eSection = document.createElement("section");
      eSection.setAttribute("class", "Ahorcado_teclado");
      for (var i = 0; i < this.teclas.length; i++) {
        var eTecla = document.createElement("div");
        eTecla.addEventListener("click", Ahorcado.clickboton);
        eTecla.innerHTML = "<p class=\"Ahorcado_tecla Ahorcado_teclaSinUsar\">" + this.teclas[i] + "</p>";
        eSection.appendChild(eTecla);
        eTecla.setAttribute("class", "Ahorcado_tecla"); 
        eTecla.addEventListener("click", function(){this.removeEventListener("click", Ahorcado.clickboton)});
      }
      div.appendChild(eSection);
    },
      
    botonReiniciar: function (div){
        var botonR = document.createElement("div");
        botonR.setAttribute("id", "reiniciar");
        botonR.innerHTML = "<p>REINICIAR</p>";
        botonR.addEventListener("click", Ahorcado.reinicio);
        div.appendChild(botonR);
    }, 
      
    reinicio: function(){
      location.reload(true);
      
  }  
     
      
      
/*function loadDoc() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     document.getElementById("demo").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", "ajax_info.txt", true);
  xhttp.send();
} */
   

  };
